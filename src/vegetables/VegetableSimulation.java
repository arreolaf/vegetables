/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetables;

import java.util.ArrayList;

/**
 *
 * @author mymac
 */
public class VegetableSimulation {
    
     
    public static void main(String[] args) {
        
        Beet ripeBeet = new Beet("red",2,"Beet");
        Beet notRipeBeet = new Beet("blue",5,"Beet");
        
        Carrot ripeCarrot = new Carrot("orange", 1.5,"Carrot");
        Carrot notRipeCarrot = new Carrot("green", 6,"Carrot");
        
        ArrayList <Vegetable> vegetables = new ArrayList<>();
     
        vegetables.add(ripeBeet);
        vegetables.add(notRipeBeet);
        vegetables.add(ripeCarrot);
        vegetables.add(notRipeCarrot);
     
        for(int i =0; i< vegetables.size(); i++){
            System.out.println(vegetables.get(i).toString());
       
        }
    
    
    }
}
