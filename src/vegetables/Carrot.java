/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetables;
/**
 *
 * @author mymac
 */
public class Carrot extends Vegetable {
    private String name;
    
    Carrot(String color,double size,String name){
    super(color,size);
    this.name = name;
    }
    
    public String getName(){
        return name;
    }
    
    public String getColor(){
        return super.getColor();
    }
    
    public double getSize(){
        return super.getSize();
    }
    
    @Override
    public boolean isRipe() {
        if (getSize() == 1.5 && getColor() == "orange"){
            return true; 
    }else{
            return false;
        }
    
    }

   public String toString(){
        return "Name is: " + getName() + " ,color is: " + getColor() +" the size is: "+ getSize()+"cm" +" ,it is ripe: "+ isRipe();
    }
    
}
