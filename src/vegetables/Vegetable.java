/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vegetables;

/**
 *
 * @author mymac
 */
public abstract class Vegetable {
    
    private String color;
    private double size;
    
    
    Vegetable(String color, double size){
        this.color = color;
        this.size = size;
    }
    
    public String getColor(){
        return color;
    }
    
    public double getSize(){
        return size;
    }
    
    public abstract boolean isRipe();
    
}
